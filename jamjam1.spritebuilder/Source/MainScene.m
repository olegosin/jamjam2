//
//  MainScene.m
//  PROJECTNAME
//
//  Created by Viktor on 10/10/13.
//  Copyright (c) 2013 Apportable. All rights reserved.
//

#import "MainScene.h"
#import "GroundModel.h"
#include <stdlib.h>

static int g_score;

@implementation MainScene {
    CCSprite* _groundCube1;
    CCSprite* _groundCube2;
    CCSprite* _groundCube3;
    CCSprite* _groundCube4;
    CCSprite* _groundCube5;
    CCSprite* _groundCube6;
    CCSprite* _groundCube7;
    
    CCSprite* _bg1;
    CCSprite* _bg2;

    float _largestGroundCubeWidth;
    
    CCButton* _startButton;
    
    GroundModel* _groundModel;
    
    CCSprite* _playerShip;
    CCSprite* _shipThruster;
    CCParticleSystem* _deathAnim;
    CGPoint _playerShipGravity;
    CGPoint _playingShipStartingPos;
    
    CCLabelTTF* _score;
    
    CCPhysicsNode* _physicsNode;
    
    BOOL _applyForceToPlayerShip;
    
    NSArray* _ground;
    NSArray* _bottom;
    NSArray* _top;
    
    int _lastBottomCubeIndex;
    int _lastTopIndex;
    
    BOOL _play;
    float _velocity;
}

- (id)init
{
    if(self = [super init]) {
        _velocity = -100.0f;
        _groundModel = [[GroundModel alloc] init];
    }
    
    return self;
}

- (void)setupScene
{
    _ground = [[NSArray alloc] initWithObjects:_groundCube1, _groundCube2, _groundCube3, _groundCube4,
               _groundCube5, _groundCube6, _groundCube7, nil];
    
    // 2, 3, 7, 5
    _bottom = [[NSArray alloc] initWithObjects:_groundCube2, _groundCube3, _groundCube7, _groundCube5, nil];
    _lastBottomCubeIndex = [_bottom count] - 1;

    // 6, 1, 4
    _top = [[NSArray alloc] initWithObjects:_groundCube6, _groundCube1, _groundCube4, nil];
    _lastTopIndex = [_top count] - 1;

    [[OALSimpleAudio sharedInstance] playEffect:@"ccbResources/bgmusic.wav" volume:1.0f pitch:1.0f pan:0.0f loop:YES];
    
    // register touches
    self.userInteractionEnabled = YES;
    
    _playerShipGravity = _playerShip.physicsNode.gravity;
    _playerShipGravity.y = -200.0f;
    _playerShip.physicsNode.gravity = CGPointZero;
    _playerShip.physicsNode.debugDraw = NO;
    _playerShip.physicsBody.collisionType = @"playerShip";
    _shipThruster.visible = NO;
    _deathAnim.visible = NO;
    _playingShipStartingPos = _playerShip.position;

    _bg1.position = CGPointMake(0.0f, 0.0f);
    
    _physicsNode.collisionDelegate = self;
    
    // find the largest ground peice
    for(CCSprite* groundObject in _ground)
    {
        if(_largestGroundCubeWidth < groundObject.boundingBox.size.width)
            _largestGroundCubeWidth = groundObject.boundingBox.size.width;
    }
    
    int score = 0;
    NSString* prevScore = _score.string;
    if([prevScore isEqualToString:@""] == NO)
    {
        score = prevScore.intValue;
    }
    if(g_score >= score)
        _score.string = [NSString stringWithFormat:@"Score: %i", g_score];
    
    [self schedule:@selector(sceneUpdate:) interval:1.0f/60.0f];
}

- (void)didLoadFromCCB
{
    [self setupScene];
}

- (void)handleStartButton
{
    if(_play == NO)
    {
        _play = YES;
        
        [_playerShip.physicsNode setGravity:_playerShipGravity];
        [_playerShip.physicsNode.physicsBody applyForce:CGPointMake(0.0f, -100.0f)];
        
        _startButton.visible = NO;
        
        _score.position = CGPointMake(20, self.boundingBox.size.height-20);
    }
}

- (void)sceneUpdate:(CCTime)interval
{
    if(_play == NO)
        return;
    
    [self updatePlayerShip];
    
    [_groundModel update:interval];
    
    [self updateScore:interval];
    [self scrollCubes:interval];
    [self recycleCubes];
    [self updateBackground:interval];
}

- (void)updateBackground:(CCTime)interval
{
    const float distanceToMove = 5.0f;
    BOOL negative = NO;
    if(_bg1.position.x >= 0.0f)
        negative = YES;
    
    float moveBy = distanceToMove * interval;
    moveBy = (negative) ? moveBy * -1 : moveBy;
    
    CCAction* moveAction = [CCActionMoveBy actionWithDuration:0 position:ccp(moveBy,0)];
    [_bg1 runAction:moveAction];

    negative = NO;
    if(_bg2.position.x >= 0.0f)
        negative = YES;

    moveBy = distanceToMove * interval;
    moveBy = (negative) ? moveBy * -1 : moveBy;
    
    CCAction* moveAction2 = [CCActionMoveBy actionWithDuration:0 position:ccp(moveBy,0)];
    [_bg2 runAction:moveAction2];

    return;
}

- (void)scrollCubes:(CCTime)interval
{
    for(CCSprite* cube in _ground)
    {
        CGPoint pos = cube.position;
        float diff = _groundModel.difficultyScale * 0.25;
        if(diff < 1.0f)
            diff = 1.0f;
        
        pos.x += _velocity * diff * interval;
        [cube setPosition:pos];
    }
}

- (void)recycleCubes
{
    float sceneWidth = self.boundingBox.size.width;
    
    for(CCSprite* cube in _bottom)
    {
        if((cube.position.x - cube.boundingBox.size.width / 2) + cube.boundingBox.size.width < 0)
        {
            CGPoint pos = cube.position;
            
            CCSprite* lastCube = [_bottom objectAtIndex:_lastBottomCubeIndex];
            
            
            pos.x = _groundModel.xPosition + lastCube.position.x + lastCube.boundingBox.size.width / 2 + (cube.boundingBox.size.width / 2);
            if(pos.x - cube.boundingBox.size.width / 2 < sceneWidth)
                pos.x = sceneWidth + cube.boundingBox.size.width / 2;
            
            pos.y += _groundModel.yPosition;

            [cube setPosition:pos];
            _lastBottomCubeIndex = [_bottom indexOfObject:cube];
            
            if(_lastBottomCubeIndex < 0)
                _lastBottomCubeIndex = [_bottom count] - 1;
        }
    }
    
    for(CCSprite* cube in _top)
    {
        if((cube.position.x - cube.boundingBox.size.width / 2) + cube.boundingBox.size.width < 0)
        {
            CGPoint pos = cube.position;
            
            CCSprite* lastCube = [_top objectAtIndex:_lastTopIndex];
            
            
            pos.x = _groundModel.xPosition + lastCube.position.x + lastCube.boundingBox.size.width / 2 + (cube.boundingBox.size.width / 2);
            if(pos.x - cube.boundingBox.size.width / 2 < sceneWidth)
                pos.x = sceneWidth + cube.boundingBox.size.width / 2;
            
            pos.y += _groundModel.yPosition;
            
            [cube setPosition:pos];
            _lastTopIndex = [_top indexOfObject:cube];
            
            if(_lastTopIndex < 0)
                _lastTopIndex = [_top count] - 1;
        }
    }
}

- (void)updatePlayerShip
{
    if(_applyForceToPlayerShip == NO)
        return;
    
    //[_playerShip.physicsNode.physicsBody applyForce:CGPointMake(0.0f, 2000.0f)];
    [_playerShip.physicsBody applyForce:CGPointMake(0.0f, 3000.0f)];
}

- (void)updateScore:(CCTime) interval
{
    if(_deathAnim.visible == YES)
        return;
    
    g_score = (int)(_groundModel.duration * _groundModel.difficultyScale);
    
    _score.string = [NSString stringWithFormat:@"%i", g_score];
}

#pragma mark touches

- (void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    
    if(_applyForceToPlayerShip == NO)
    {
        [[OALSimpleAudio sharedInstance] playEffect:@"ccbResources/thruster.wav" volume:0.02f pitch:0.5f pan:0.0f loop:NO];
    }
    
    _applyForceToPlayerShip = YES;
    _shipThruster.visible = YES;
}

- (void)touchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    _applyForceToPlayerShip = NO;
    _shipThruster.visible = NO;
}

#pragma mark collision

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair playerShip:(CCNode *)nodeA wildcard:(CCNode *)nodeB
{
    if(_play == NO)
        return;
    
    _deathAnim.visible = YES;

    [[OALSimpleAudio sharedInstance] playEffect:@"ccbResources/death.wav" volume:0.02f pitch:1.0f pan:0.0f loop:NO];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [[OALSimpleAudio sharedInstance] stopEverything];
        [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainScene"]];
    });
}


@end
