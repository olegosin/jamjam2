//
//  GroundModel.m
//  jamjam1
//
//  Created by Oleg Osin on 1/30/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "GroundModel.h"
#include <stdlib.h>

#define DIFFICUTLY_INTERVAL 5.0f

@implementation GroundModel {
}

- (id)init
{
    if(self = [super init])
    {
        return self;
    }
    
    return self;
}

- (void)update:(CCTime)interval
{
    _duration += interval;
}

- (float)yPosition
{
    int fromNumber = 0;
    int toNumber = 10;
    int randomNumber = (arc4random()%(toNumber-fromNumber))+fromNumber;
    
    int negative = arc4random() % 2;
    if(negative == 1)
        randomNumber *= -1;

    float scale = self.difficultyScale;
    _yPosition = randomNumber * scale;

    return _yPosition;
    
    //return (float)randomNumber / 100.0f + 1.0f; // use this if i want a %
}

- (float)xPosition
{
    int fromNumber = 50;
    int toNumber = 100;
    int randomNumber = (arc4random()%(toNumber-fromNumber))+fromNumber;
    
    _xPosition = randomNumber;
    
    return _xPosition;
}

- (int)rotation
{
    int fromNumber = 1;
    int toNumber = 10;
    int randomNumber = (arc4random()%(toNumber-fromNumber))+fromNumber;
    
    return randomNumber;
}

- (float)difficultyScale
{
    if(_duration < DIFFICUTLY_INTERVAL)
        _difficultyScale = 1.0f;
    else
    {
        _difficultyScale = _duration / DIFFICUTLY_INTERVAL;
    }
    
    return _difficultyScale;
}

- (void)reset
{
    _duration = 0.0f;
}

@end
