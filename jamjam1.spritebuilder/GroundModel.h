//
//  GroundModel.h
//  jamjam1
//
//  Created by Oleg Osin on 1/30/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode.h"

@interface GroundModel : NSObject

@property (nonatomic) float yPosition;
@property (nonatomic) float xPosition;
@property (nonatomic) int rotation;

@property (nonatomic) float difficultyScale;
@property (nonatomic, readonly) CCTime duration;

- (void)update:(CCTime)interval;

- (void)reset;

@end
